# Camera_Kotlin

Projeto para testar as funcionalidades da linguagem Kotlin usando a câmera do Android

# Funcionalidades

Atualmente ele mostra uma tela com um icone e um botão abaixo escrito "Tirar foto". Quando o botão é apertado o programa tira uma foto, salva ela em uma pasta especifica do programa, e mostra a foto no lugar do ícone.  
Botão "Escolher foto" adicionado. Quando o botão é pressionado o usuário escolhe uma imagem e tal imagem vai ser mostrada.  
Botão "Rotacionar 90° Esquerda" rotaciona a imagem 90° a esquerda.  
Botão "Rotacionar 90° Direita" rotaciona a imagem 90° a direita.  
Botão "180°" rotaciona a imagem 180°.  

# Referências
https://developer.android.com/training/camera/photobasics#kotlin  
https://stackoverflow.com/questions/4181774/show-image-view-from-file-path  
https://stackoverflow.com/questions/11346275/android-why-is-using-onsaveinsancestate-to-save-a-bitmap-object-not-being-ca  
https://www.youtube.com/playlist?list=PLhPyEFL5u-i1Gz_U-msxrHuWsTHbmwp24  
https://devofandroid.blogspot.com/2018/09/pick-image-from-gallery-android-studio_15.html  
