package com.example.camera

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.PersistableBundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.toBitmap
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {
    //code used to identify a camera intent
    private val PERMISSION_CODE: Int = 1001
    private val IMAGE_PICK_CODE: Int = 1000
    val REQUEST_TAKE_PHOTO = 1

    //Path to the current Photo taken (Updated every time a new photo is taken)
    var currentPhotoPath: String? = null

    //Widget's variables
    private var buttonTakePhoto: Button? = null
    private var buttonChoosePhoto: Button? = null
    private var buttonRotate90l: Button? = null
    private var buttonRotate90r: Button? = null
    private var buttonRotate180: Button? = null
    private var imageView: ImageView? = null
    var bitmapArray : ArrayList<Bitmap>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonTakePhoto = findViewById(R.id.takePhoto)
        buttonChoosePhoto = findViewById(R.id.choosePhoto)
        buttonRotate90l = findViewById(R.id.rotate_90l)
        buttonRotate90r = findViewById(R.id.rotate_90r)
        buttonRotate180 = findViewById(R.id.rotate_180)
        imageView = findViewById(R.id.imageView)


        //Listener do botão "Tirar Foto"
        buttonTakePhoto?.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ) {
                    // Permission is not granted
                    val permission = arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    // Ask permission
                    requestPermissions(permission, PERMISSION_CODE)
                } else {
                    // Permision already granted
                    dispatchTakePictureIntent()
                }
            } else {
                // System OS < marshmallow
                dispatchTakePictureIntent()
            }
        }

        //Listener do botão "Escolher Foto"
        buttonChoosePhoto?.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                } else{
                    //permission already granted
                    pickImageFromGallery();
                }
            } else{
                //system OS is < Marshmallow
                pickImageFromGallery();
            }
        }

        buttonRotate90l?.setOnClickListener{
            var bitmap: Bitmap? = (imageView?.drawable)?.toBitmap()
            if (bitmap != null) {
                bitmap = RotateBitmap(bitmap, 90F)
            }
            imageView?.setImageBitmap(bitmap)
        }

        buttonRotate90r?.setOnClickListener{
            var bitmap: Bitmap? = (imageView?.drawable)?.toBitmap()
            if (bitmap != null) {
                bitmap = RotateBitmap(bitmap!!, 270F)
            }
            imageView?.setImageBitmap(bitmap)
        }

        buttonRotate180?.setOnClickListener{
            var bitmap: Bitmap? = (imageView?.drawable)?.toBitmap()
            if (bitmap != null) {
                bitmap = RotateBitmap(bitmap!!, 180F)
            }
            imageView?.setImageBitmap(bitmap)
        }

        //Check if the bundle exists
        if(savedInstanceState != null) {
            //Get the bitmap from the bundle
            var bitmap: Bitmap? = savedInstanceState.getParcelable("bitmap");
            Log.d("STATE-RESTORE", "bitmap created");
            imageView?.setImageBitmap(bitmap)
        }

    }

    //Starts a picture intent
    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    Toast.makeText(this, "Something went wrong. Could not create file.", Toast.LENGTH_SHORT).show()
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.example.camera.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            setPic()
            galleryAddPic()
        }
        if (resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE){
            imageView?.setImageURI(data?.data)
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    private fun galleryAddPic() {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(currentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            sendBroadcast(mediaScanIntent)
        }
    }

    private fun setPic() {
        // Get the dimensions of the View
        val targetW: Int? = imageView?.width
        val targetH: Int? = imageView?.height

        val bmOptions = BitmapFactory.Options().apply {
            // Get the dimensions of the bitmap
            inJustDecodeBounds = true

            val photoW: Int = outWidth
            val photoH: Int = outHeight

            // Determine how much to scale down the image
            val scaleFactor: Int = Math.min(photoW / targetW!!, photoH / targetH!!)

            // Decode the image file into a Bitmap sized to fill the View
            inJustDecodeBounds = false
            inSampleSize = scaleFactor
            inPurgeable = true
        }
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions)?.also { bitmap ->
            imageView?.setImageBitmap(bitmap)
        }
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

        // Save the values you need into "outState"
        var bitmap: Bitmap? = (imageView?.drawable)?.toBitmap()
        outState.putParcelable("bitmap", bitmap);
        Log.d("STATE-SAVE", "onSaveInstanceState()");
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun RotateBitmap(source: Bitmap, angle: Float) : Bitmap{
        var matrix : Matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }
}
